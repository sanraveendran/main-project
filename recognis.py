import cv2
import numpy as np
import os
from gtts import gTTS
from PIL import Image
import sqlite3
from sqlite3 import Error

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer/trainer.yml')
#cascadePath = "haarcascade_frontalface_default.xml"
# faceCascade = cv2.CascadeClassifier(cascadePath);
faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
#face_cascade = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')
font = cv2.FONT_HERSHEY_SIMPLEX
_id = 0
_co = 0
_m = 0
_ids1 = []

#_names = ['None', 'sandra', 'basil', 'arya', 'devika','vishnu', 'ajay', 'jini', 'RAHUL K', 'Devi', 'roshna', 'ambili','arya', 'shereena','reshma', 'meenu', 'aneesh', 'aelias', 'gireesh', 'swathy', 'haritha','Tomsy'] 
_name =[]

cam = cv2.VideoCapture(0)
cam.set(3, 640)  
cam.set(4, 480) 


minW = 0.1*cam.get(3)
minH = 0.1*cam.get(4)

connection = sqlite3.connect("/home/pi/sensordata.db")
cursor = connection.cursor()
sql = """SELECT name FROM face_names"""
cursor.execute(sql)
records = list(cursor.fetchall())
#record = data.select(sql)
#for record in records:
_names = records
connection.commit()
connection.close()
print(_names)
print(records)

def callme(mytext):
    _te = 'hello' + str(mytext)+ 'how are you'
    _language = 'en'
    _myjob = gTTS(text=_te, lang=_language, slow=False)
    _myjob.save("welcome.mp3")
    os.system("mpg321 welcome.mp3")
    return None

def captureme():
    
    cam = cv2.VideoCapture(0)
    cam.set(3, 640) # set video width
    cam.set(4, 480) # set video height
    minW = 0.1*cam.get(3)
    minH = 0.1*cam.get(4)
    face_detector = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    #face_cascade = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')
# For each person, enter one numeric face id
    _l = len(_names)
    #face_id = input('\n enter user id end press <return> ==>  ')
    face_id = _l
    print(_l)
    print('your unique id is ',face_id)
    _language = 'en'
    _te = 'your unique id is' + str(face_id) + 'Initializing face capture. Look the camera and wait'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("msg2.mp3")
    os.system("mpg321 msg2.mp3")
    print("\n [INFO] Initializing face capture. Look the camera and wait ...")
# Initialize individual sampling face count
    count = 0

    while(True):

        ret, img = cam.read()
        img = cv2.flip(img, -1) # flip video image vertically
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_detector.detectMultiScale(gray, 1.2, 5)

        for (x,y,w,h) in faces:

            cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 2)     
            count += 1

        # Save the captured image into the datasets folder
            cv2.imwrite("dataset/User." + str(face_id) + '.' + str(count) + ".jpg", gray[y:y+h,x:x+w])

            cv2.imshow('image', img)

        k = cv2.waitKey(100) & 0xff # Press 'ESC' for exiting video
        if k == 27:
            break
        elif count >= 30: # Take 30 face sample and stop video
            break
    _language = 'en'
    _te = 'Enter your name please'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("msg3.mp3")
    os.system("mpg321 msg3.mp3")
    face_name = input('Enter name')
    print(face_name)
    connection = sqlite3.connect("/home/pi/sensordata.db")
    cursor = connection.cursor()
    cursor.execute("INSERT INTO face_names VALUES (?, ?)",(face_id, face_name))
#record = data.select(sql)
#for record in records:
    connection.commit()
    connection.close()
    #names.append(face_name)
    print(_names)
    _te = 'Let me train your images please . This will take time'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("instruct.mp3")
    os.system("mpg321 instruct.mp3")
    trainthis()
    return None            

def trainthis():

    _path = 'dataset'

    recognizer = cv2.face.LBPHFaceRecognizer_create()
    detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
#     detector = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')

    def getImagesAndLabels(_path):

        _imagePaths = [os.path.join(_path,f) for f in os.listdir(_path)]     
        _faceSamples=[]
        _ids = []
        

        for _imagePath in _imagePaths:
            PIL_img = Image.open(_imagePath).convert('L') 
            img_numpy = np.array(PIL_img,'uint8')

            id = int(os.path.split(_imagePath)[-1].split(".")[1])
            _faces = detector.detectMultiScale(img_numpy)

            for (x,y,w,h) in _faces:
                _faceSamples.append(img_numpy[y:y+h,x:x+w])
                _ids.append(id)

        return _faceSamples,_ids

    print ("\n [INFO] Training faces. It will take a few seconds.")
    _language = 'en'
    _te = 'raining faces. It will take a few seconds. You my go.catch you up later, have a nice day bye'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("msg1.mp3")
    os.system("mpg321 msg1.mp3")
    _faces, _ids = getImagesAndLabels(_path)
    recognizer.train(_faces, np.array(_ids))

    recognizer.write('trainer/trainer.yml') 
    print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(_ids))))
    _te = 'faces trained'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("msg3.mp3")
    os.system("mpg321 msg3.mp3")
    cam.release()
    cv2.destroyWindow('image')
    
def opencam():
    cam = cv2.VideoCapture(0)
    cam.set(3, 640)  
    cam.set(4, 480) 


    minW = 0.1*cam.get(3)
    minH = 0.1*cam.get(4)
    return None
#_path = 'dataset'
#_imagePaths = [os.path.join(_path,f) for f in os.listdir(_path)] 
#print(len(_imagePaths))
#g = len(_imagePaths)
#imglength = int((g + 2) / 30)
#print (imglength)
print(_names)
while True:
    
    ret, img = cam.read()
    img = cv2.flip(img, -1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale( 
        gray,
        scaleFactor = 1.2,
        minNeighbors = 5
        ,
        minSize = (int(minW), int(minH))
       )
    
    for(x,y,w,h) in faces:
        
        cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)
        _id, _confidence = recognizer.predict(gray[y:y+h,x:x+w])
        
        
        if (_confidence < 60):
            print(_id)
            _id = _names[_id]
            if _m == _id:
                pass
            else:
                callme(_id)
                _confidence = "  {0}%".format(round(_confidence))
                _m = _id
            cv2.putText(img, str(_id), (x+5,y-5), font, 1, (255,255,255), 2)
            cv2.putText(img, str(_confidence), (x+5,y+h-5), font, 1, (255,255,0), 1) 
        else:
            _id = "unknown"
            _co += 1
            _confidence = "  {0}%".format(round(_confidence))
            cv2.putText(img, str(_id), (x+5,y-5), font, 1, (255,255,255), 2)
            cv2.putText(img, str(_confidence), (x+5,y+h-5), font, 1, (255,255,0), 1) 
            if _co == 20:
                te = 'sorry i don\'t know you'
                language = 'en'
                myjob = gTTS(text=te, lang=language, slow=True)
                myjob.save("msg.mp3")
                os.system("mpg321 msg.mp3") 
                cam.release()
                cv2.destroyAllWindows()
                captureme()
                cam = cv2.VideoCapture(0)
                cam.set(3, 640) # set video width
                cam.set(4, 480) # set video height
                minW = 0.1*cam.get(3)
                minH = 0.1*cam.get(4)
                _co = 0
                break
            
            continue
    cv2.imshow('camera',img) 
    
    k = cv2.waitKey(10) & 0xff 
    if k == 27:
        break
    
    

print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()
      
