#!/usr/bin/env python3
from imutils.video import VideoStream
from imutils.video import FPS
from imutils import paths
from gtts import gTTS
import face_recognition
import argparse
import imutils
import pickle
import time
import RPi.GPIO as GPIO 
import cv2
import os
# import ultrasonicrange1 
import mainmain
import train
import capture
import call

GPIO.setmode(GPIO.BCM)
# GPIO.setup(m12,GPIO.OUT)
# GPIO.setup(m21,GPIO.OUT)
GPIO_SIG = 11
m11 = 16
m12 = 12
m21 = 21
m22 = 20
GPIO.setup(16,GPIO.OUT)
GPIO.setup(12,GPIO.OUT)
GPIO.setup(21,GPIO.OUT)
GPIO.setup(20,GPIO.OUT)
time.sleep(5)
p = GPIO.PWM(16,100)
q = GPIO.PWM(21,100)
r = GPIO.PWM(12,100)
s = GPIO.PWM(20,100)  
p.start(0)
q.start(0)
r.start(0)
s.start(0)
# runmain()
def stops():
    p.ChangeDutyCycle(0)
    q.ChangeDutyCycle(0)
    r.ChangeDutyCycle(0)
    s.ChangeDutyCycle(0)
    print ("stop")
    GPIO.output(m11, 0)
    GPIO.output(m12, 0)
    GPIO.output(m21, 0)
    GPIO.output(m22, 0)
def forward():
    p.ChangeDutyCycle(30)
    q.ChangeDutyCycle(30)
    GPIO.output(m11, 1)
    GPIO.output(m12, 0)
    GPIO.output(m21, 1)
    GPIO.output(m22, 0)
    print ("Forward")

def back():
    r.ChangeDutyCycle(30)
    s.ChangeDutyCycle(30)
    GPIO.output(m11, 0)
    GPIO.output(m12, 1)
    GPIO.output(m21, 0)
    GPIO.output(m22, 1)
    print ("back")

def left():
    q.ChangeDutyCycle(30)
    GPIO.output(m11, 0)
    GPIO.output(m12, 0)
    GPIO.output(m21, 1)
    GPIO.output(m22, 0)
    print ("left")

def right():
    p.ChangeDutyCycle(30)
    GPIO.output(m11, 1)
    GPIO.output(m12, 0)
    GPIO.output(m21, 0)
    GPIO.output(m22, 0)
    print ("right")

def runrun():

    stops()
    counts = 0
    try:
        while True:
            i=0
            avgDistance=0
                                 #Delay
            GPIO.setup(GPIO_SIG, GPIO.OUT)
            GPIO.output(GPIO_SIG, GPIO.LOW)
            time.sleep(0.2)
            GPIO.output(GPIO_SIG, GPIO.HIGH)
            time.sleep(0.5)
            GPIO.output(GPIO_SIG, GPIO.LOW)
            start = time.time()

    # setup GPIO_SIG as input
            GPIO.setup(GPIO_SIG, GPIO.IN)

    # get duration from Ultrasonic SIG pin
            while GPIO.input(GPIO_SIG) == 0:
                start = time.time()

            while GPIO.input(GPIO_SIG) == 1:
                stopp = time.time()
      
            print ("Ultrasonic Measurement")

    # Calculate pulse length
            elapsed = stopp-start

    # Distance pulse travelled in that time is time
    # multi plied by the speed of sound (cm/s)
            distance = elapsed * 34300

    # That was the distance there and back so halve the value
            distance = distance / 2
      
            print ("Distance : %.1f CM" % distance)
            print (distance)
            flag=0
            if distance < 15:      #Check whether the distance is within 15 cm range
                stops()
                counts=counts+1
                time.sleep(1.5)
                back()
                time.sleep(0.5)
                runmain()
                if (counts%3 ==1) & (flag==0):
                    right()
                    flag=1
                else:
                    left()
                    flag=0
                time.sleep(1.5)
                stops()
                time.sleep(1.5)
            else:
                forward()
                flag=0

    except KeyboardInterrupt:
        GPIO.cleanup()
    return None

def runmain() :
    mainmain.main()
    count = 0
    countm = 0
    countn = 0
    while True:
    # grab the frame from the threaded video stream and resize it
    # to 500px (to speedup processing)
        countn += 1
        frame = mainmain.vs.read()
#     time.sleep(2)
        frame = cv2.flip(frame, -1)
        frame = imutils.resize(frame, width=500)

    # convert the input frame from (1) BGR to grayscale (for face
    # detection) and (2) from BGR to RGB (for face recognition)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # detect faces in the grayscale frame
        rects = mainmain.detector.detectMultiScale(gray, scaleFactor=1.1,
            minNeighbors=15, minSize=(60, 60),
            flags=cv2.CASCADE_SCALE_IMAGE)

    # OpenCV returns bounding box coordinates in (x, y, w, h) order
    # but we need them in (top, right, bottom, left) order, so we
    # need to do a bit of reordering
        boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]
        if countn == 30 :
            countn = 0
            mainmain.vs.stream.release()
            cv2.destroyAllWindows()
            mainmain.vs.stop()
            time.sleep(1)
            try: x
            except NameError: runrun()
            else: countn = 0
        else:
            pass
    # compute the facial embeddings for each face bounding box
        encodings = face_recognition.face_encodings(rgb, boxes)
        names = []
    # loop over the facial embeddings

        for encoding in encodings:
        # attempt to match each face in the input image to our known
        # encodings

            matches = face_recognition.compare_faces(mainmain.data["encodings"],
                encoding)
            name = "Unknown"

        # check to see if we have found a match
            if True in matches:
            # find the indexes of all matched faces then initialize a
            # dictionary to count the total number of times each face
            # was matched
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}

            # loop over the matched indexes and maintain a count for
            # each recognized face face
                for i in matchedIdxs:
                    name = mainmain.data["names"][i]
                    counts[name] = counts.get(name, 0) + 1

            # determine the recognized face with the largest number
            # of votes (note: in the event of an unlikely tie Python
            # will select first entry in the dictionary)
                name = max(counts, key=counts.get)

        # update the list of names
            names.append(name)

    # loop over the recognized faces

        for ((top, right, bottom, left), name) in zip(boxes, names):
        # draw the predicted face name on the image
            cv2.rectangle(frame, (left, top), (right, bottom),
                (0, 255, 0), 2)
            y = top - 15 if top - 15 > 15 else top + 15
            cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                0.75, (0, 255, 0), 2)

            if name == "Unknown":
                count += 1
                if count != 5:
                    print(count)
                    pass
                else:
                    count = 0
                    te = 'sorry i don\'t know you'
                    language = 'en'
                    myjob = gTTS(text=te, lang=language, slow=True)
                    myjob.save("msg.mp3")
                    os.system("mpg321 msg.mp3")
                    mainmain.vs.stream.release()
                    mainmain.vs.stop()
                    cv2.destroyAllWindows()
                    capture.captureme()
            else:
                countm += 1
                print(countm)
                if countm == 5:
                    countm = 0
                    call.callme(name)
                else:
                    pass
    # display the image to our screen
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    # update the FPS counter
        mainmain.fps.update()

# stop the timer and display FPS information
    mainmain.fps.stop()
    print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
    cv2.destroyAllWindows()
    mainmain.vs.stop()
    return None

if __name__ == '__main__' :

    GPIO.setwarnings(False)

#         p.start(0)
#     q.start(0)
    
    runmain()

        


   
