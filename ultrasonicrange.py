#!/usr/bin/env python3
import RPi.GPIO as GPIO                    #Import GPIO library
import time

#Import time library
GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)


m11 = 16
m12 = 12
m21 = 21
m22 = 20

GPIO_SIG = 11


GPIO.setup(m11,GPIO.OUT)
GPIO.setup(m12,GPIO.OUT)
GPIO.setup(m21,GPIO.OUT)
GPIO.setup(m22,GPIO.OUT)

time.sleep(5)

def stop():
    
    print ("stop")
    GPIO.output(m11, 0)
    GPIO.output(m12, 0)
    GPIO.output(m21, 0)
    GPIO.output(m22, 0)
def forward():
    
    GPIO.output(m11, 1)
    GPIO.output(m12, 0)
    GPIO.output(m21, 1)
    GPIO.output(m22, 0)
    print ("Forward")

def back():

    GPIO.output(m11, 0)
    GPIO.output(m12, 1)
    GPIO.output(m21, 0)
    GPIO.output(m22, 1)
    print ("back")

def left():
    
    GPIO.output(m11, 0)
    GPIO.output(m12, 0)
    GPIO.output(m21, 1)
    GPIO.output(m22, 0)
    print ("left")

def right():
    
    GPIO.output(m11, 1)
    GPIO.output(m12, 0)
    GPIO.output(m21, 0)
    GPIO.output(m22, 0)
    print ("right")



stop
count = 0
while True:
    i=0
    avgDistance=0
                                 #Delay
    GPIO.setup(GPIO_SIG, GPIO.OUT)
    GPIO.output(GPIO_SIG, GPIO.LOW)
    time.sleep(0.2)
    GPIO.output(GPIO_SIG, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(GPIO_SIG, GPIO.LOW)
    start = time.time()

    # setup GPIO_SIG as input
    GPIO.setup(GPIO_SIG, GPIO.IN)

    # get duration from Ultrasonic SIG pin
    while GPIO.input(GPIO_SIG) == 0:
        start = time.time()

    while GPIO.input(GPIO_SIG) == 1:
        stop = time.time()
      
    print ("Ultrasonic Measurement")

    # Calculate pulse length
    elapsed = stop-start

    # Distance pulse travelled in that time is time
    # multi plied by the speed of sound (cm/s)
    distance = elapsed * 34300

    # That was the distance there and back so halve the value
    distance = distance / 2
      
    print ("Distance : %.1f CM" % distance)
    print (distance)
    flag=0
    if distance < 15:      #Check whether the distance is within 15 cm range
        count=count+1
        stop
        time.sleep(1)
        back()
        time.sleep(1)
        if (count%3 ==1) & (flag==0):
            right()
            flag=1
        else:
            left()
            flag=0
        time.sleep(1)
        stop
        time.sleep(1)
    else:
        forward()
        flag=0



