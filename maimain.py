from imutils.video import VideoStream
from imutils.video import FPS
import face_recognition
import argparse
import imutils
import pickle
import time
import cv2
def main():
    global data
    global detector
    global vs
    global fps
    print("[INFO] loading encodings + face detector...")
    data = pickle.loads(open("encodings", "rb").read())
    detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    print("[INFO] starting video stream...")
# vs = VideoStream(usePiCamera=True).start()
    vs = VideoStream(src=0).start()
    time.sleep(2.0)
# start the FPS counter
    fps = FPS().start()
    return None