import cv2
import numpy as np
from PIL import Image
import os


_path = 'dataset1'

recognizer = cv2.face.FisherFaceRecognizer_create()
detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")


def getImagesAndLabels(_path):

    _imagePaths = [os.path.join(_path,f) for f in os.listdir(_path)]     
    _faceSamples=[]
    _ids = []

    for _imagePath in _imagePaths:

        PIL_img = Image.open(_imagePath).convert('L') 
        img_numpy = np.array(PIL_img,'uint8')

        id = int(os.path.split(_imagePath)[-1].split(".")[1])
        _faces = detector.detectMultiScale(img_numpy)

        for (x,y,w,h) in _faces:
            _faceSamples.append(img_numpy[y:y+h,x:x+w])
            _ids.append(id)

    return _faceSamples,_ids

print ("\n [INFO] Training faces. It will take a few seconds. Wait ...")
_faces, _ids = getImagesAndLabels(_path)
recognizer.train(_faces, np.array(_ids))

recognizer.write('trainer/trainer.yml') 


print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(_ids))))
