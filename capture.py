from imutils.video import VideoStream
import argparse
import imutils
import numpy as np
import time
from imutils.video import FPS
from gtts import gTTS
from PIL import Image
import pickle
import cv2
import os
import train
import mainmain



def captureme():
    global path
    detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    output = "dataset"
# initialize the video stream, allow the camera sensor to warm up,
# and initialize the total number of example faces written to disk
# thus far
    print("[INFO] starting video stream...")
    vs = VideoStream(src=0).start()
    time.sleep(2.0)
    fps = FPS().start()
# vs = VideoStream(usePiCamera=True).start()

    total = 0
    _language = 'en'
    _te = 'Enter your name please'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("msg3.mp3")
    os.system("mpg321 msg3.mp3")
    face_name = input('Enter name')
    print(face_name)
    parent="dataset"
    child =  face_name
    path = os.path.join(parent, child) 
    os.makedirs(path,exist_ok = True)
# loop over the frames from the video stream
    while True:
    # grab the frame from the threaded video stream, clone it, (just
    # in case we want to write it to disk), and then resize the frame
    # so we can apply face detection faster
        frame = vs.read()
        frame = cv2.flip(frame,-1)
        orig = frame.copy()
        frame = imutils.resize(frame, width=400)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # detect faces in the grayscale frame
        rects = detector.detectMultiScale(
            gray, scaleFactor=1.1, 
            minNeighbors=5)
    # loop over the face detections and draw them on the frame
        for (x, y, w, h) in rects:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # show the output frame
    
#     key = cv2.waitKey(1) & 0xFF
 
    # if the `k` key was pressed, write the *original* frame to disk
    # so we can later process it and use it for face recognition
    
            p = os.path.sep.join([path, "{}.png".format( str(total).zfill(5))])
            cv2.imwrite(p, gray[y:y+h+1,x:x+w+1])
            total += 1
            cv2.imshow("Frame", frame)
        k = cv2.waitKey(100) & 0xff # Press 'ESC' for exiting video
        if k == 27:
            break
        elif total >= 6: # Take 30 face sample and stop video
            break
        
    # if the `q` key was pressed, break from the loop
    mainmain.fps.stop()
# do a bit of cleanup
    print("[INFO] {} face images stored".format(total))
    print("[INFO] cleaning up...")
    vs.stream.release()
    vs.stop()
    cv2.destroyAllWindows()
    print("[INFO] loading encodings + face detector...")
    data = pickle.loads(open("encodings", "rb").read())
    detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    _te = 'Let me train your images please . This will take time'
    _myjob = gTTS(text=_te, lang=_language, slow=True)
    _myjob.save("instruct.mp3")
    time.sleep(1.0)
    os.system("mpg321 instruct.mp3")
    train.trainface()
    mainmain.main()
    return None